# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.osv.orm import Model
from openerp.osv import fields

class BaseReportFooter(Model):
    _name = 'ir.report.footer'

    _columns = {
        'name': fields.char('Name', required=True, select=1),
        'text': fields.html('Text'),
        'company_id': fields.many2one('res.company', 'Company', select=1),
    }
