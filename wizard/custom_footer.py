# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.osv.orm import TransientModel
from openerp.osv import fields

class CustomFooter(TransientModel):
    """ Helper could be use in wizard that prints reports """
    _name = 'ir.report.footer.custom'
    _description = __doc__
    _columns = {
        'footer_id': fields.many2one('ir.report.footer', 'Report Footer'),
        'footer_text': fields.html('Footer Text'),
    }

    def on_change_footer(self, cr, uid, ids, footer_id, context=None):
        values = {'footer_text': False}
        if not footer_id:
            return {'value': values}

        footer_obj = self.pool.get('ir.report.footer')
        footer_text = footer_obj.read(cr, uid, footer_id, ['text'],
                                      context=context)['text']
        values.update(footer_text=footer_text)
        return {'value': values}