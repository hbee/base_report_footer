# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.
{
    'name': 'Report Footers',
    'version': '0.1',
    'author': 'HacBee UAB',
    'category': 'Custom',
    'website': 'http://www.hbee.eu',
    'summary': '',
    'description': """
Base module to define different report footers.
Mainly used for webkit reports.
""",
    'depends': [
        'base',
    ],
    'data': [
        'security/ir.model.access.csv',
        'view/footer.xml',
    ],
    'installable': True,
    'application': False,
}
